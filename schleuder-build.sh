#!/bin/sh

set -e

apt-get -qqq update
apt-get -qqq install --no-install-recommends --no-install-suggests eatmydata git gnupg2 libgpgme11-dev libsqlite3-dev libicu-dev
rm -rf /var/lib/apt/lists/*

git clone https://0xacab.org/schleuder/schleuder
cd schleuder
git checkout main
bundle install --jobs $(nproc)

rm /dev/random
ln -s /dev/urandom /dev/random
